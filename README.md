# Movie Recommendation System
<h2>Overview</h2><p>
The code uses the [lightfm](https://github.com/lyst/lightfm) recommender system library to train a hybrid content-based + collaborative algorithm that uses the WARP loss function on the [movielens](http://grouplens.org/datasets/movielens/) dataset. The movielens dataset contains movies and ratings from over 1700 users. Once trained, our script prints out recommended movies for whatever users from the dataset that we choose to terminal.
</p>
<h2>Dependencies</h2>

* numpy (http://www.numpy.org/)
* scipy (https://www.scipy.org/)
* lightfm (https://github.com/lyst/lightfm)
Install missing dependencies using [pip](https://pip.pypa.io/en/stable/installing/)

<h2>Usage</h2>

Once you have your dependencies installed via pip, run the script in terminal via
```
python demo.py
```

**Note** If the lightfm dependency doesn't work for you via pip, just install it from source by running these two commands.

```
git clone git@github.com:lyst/lightfm.git
```
```
cd lightfm && pip install -e .
```

If you still have dependency version issues, use [virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/). 

<h2>Sample Output</h2>
<img src="https://github.com/ratansingh98/Movie_Recommendation_System/blob/master/Output/Screenshot%20from%202018-06-03%2021-25-12.png"/>

<h2>Credits</h2>

Credit goes to the [lightfm](https://github.com/lyst/lightfm) team. I've merely created a wrapper to make it more readable.
