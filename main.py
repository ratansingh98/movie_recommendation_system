# imports
import numpy as np
from lightfm.datasets import fetch_movielens
from lightfm import LightFM
from collections import Counter

# fetch data and format it
data = fetch_movielens(min_rating=4.0)

# print training data
print(repr(data['train']),"\n\n")

# List for storing movie names
known_positivesList=[]
RecommendedList=[]

# Used for recommending movies based upon model , data and user_Ids
def sample_recommendation(model, data, user_ids):

    #Number of users and movies in training data
    n_users, n_items = data['train'].shape

    # Generate recommendations for each user we input
    for user_id in user_ids:

        # Movies they already like
        known_positives = data['item_labels'][data['train'].tocsr()[user_id].indices]

        # Movies our model predicts they will like
        scores = model.predict(user_id, np.arange(n_items))
        # Rank them in order of most liked to least
        top_items = data['item_labels'][np.argsort(-scores)]

        # Known positive movies
        for x in known_positives[:]:
            known_positivesList.append(x)
        # Recommended Movies
        for x in top_items[:]:
            RecommendedList.append(x)


# Create model for warp loss
model1 = LightFM(loss='warp')
# Train model for warp loss
model1.fit(data['train'], epochs=30, num_threads=20)

# Create model for logistic loss
model2 = LightFM(loss='logistic')
# Train model for logistic loss
model2.fit(data['train'], epochs=30, num_threads=20)

# Create model for bpr loss
model3 = LightFM(loss='bpr')
# Train model for bpr loss
model3.fit(data['train'], epochs=30, num_threads=20)

# Create model for warp-kos loss
model4 = LightFM(loss='warp-kos')
# Train model for warp-kos loss
model4.fit(data['train'], epochs=30, num_threads=20)

# Take input from users
user_ids = []
num = int(input("For how many users you want to perfom this prediction\n"))
print("Enter User Ids (0-942)")
for i in range(num):
    user_ids.append(input())

# Pass the data
sample_recommendation(model1, data, user_ids)
sample_recommendation(model2, data, user_ids)
sample_recommendation(model3, data, user_ids)
sample_recommendation(model4, data, user_ids)

# Printing
print("\n\nRecommended movies are:-\n")
RecommendedMovies = Counter(RecommendedList).most_common(5)
print(*[RecommendedMovies[i][0] for i in range(5)],sep="\n")
print("\n--------------------------------")
print("\nThe Best Known Recommended movies are:-\n")
knownMovies = Counter(known_positivesList).most_common(5)
print(*[knownMovies[i][0] for i in range(5)],sep="\n")